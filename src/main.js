import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import AddVirus from './AddVirus.vue'
import Login from './Login.vue'
import Map from './components/Map.vue'
import 'leaflet/dist/leaflet.css';

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
  { path: '/', component: Map },
  { path: '/login', component: Login},
  { path: '/add-virus', component: AddVirus }
]

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
